import "@babel/polyfill";
import express from 'express';
import cors from 'cors'
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser'
import mongoose from 'mongoose';
import passport from 'passport';
import session from 'express-session';

// *** Routes ***
import users from './routes/users-routes';
import policy from './routes/policy-routes';

const app = express();

// *** Middleware ***
app.use(cookieParser());

// *** Parsing the JSON requests ***
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// app.use(session({
//     secret: 'keyboard cat',
//     resave: false,
//     saveUninitialized: true,
//    // cookie: { secure: true }
// }));

// *** User Auth ***
app.use(passport.initialize());
app.use(passport.session());
app.use(cors()); //??? what is this used for find out later



// *** MongoDB connection ***
mongoose.connect('mongodb://localhost:8080/confus');
const connection = mongoose.connection;
connection.once('open', () => {
    console.log("MongoDB connection successful")
});


//Users API Routes
app.use('/users', users);
app.use('/policy', policy);


// *** Handel 404 Errors (incorrect routes) ***
// app.use((req, res, next) => {
//     let err = new Error("Not Found");
//     err.status = 404;
//     next(err);
// });


app.listen(4000, () => console.log('Server running on port 4000'));

