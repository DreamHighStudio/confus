import Policies from '../models/policy-models'
import user_controller from './users-controller'
import Users from "../models/users-models";

var random = require('random-hex-character-generator');

async function create_policy(req, res, next) {
    let policy = new Policies();
    policy.policy_name = req.body.policyName;
    policy.ulrs = req.body.urls;
    policy.date = req.body.date;
    policy.session_ID = random.RandomHax(32);
    policy.save().then(async pol => {
        await Users.findOneAndUpdate({email: req.body.email}, {$push: {policies: pol}}, async (err, result) => {
            if (result != null) {
                // Error code 1001 duplicate account found associated with this account.
                res.json(1001)
            } else {
                res.json(202);
            }
        }).catch(err => {
            res.status(400).send("Failed to create new record");
        });
    }).catch(err => {
        res.status(400).send("Failed to create new record");
    });
}

async function get_policy_user(req, res, next) {
    await Users.findOne({email: req.body.email}, (err, result) => {
        if (result != null) {
            res.json(result.policies);
        }
    });
}

async function get_policy_session_id(req, res, next) {
    console.log(req.body.sessionID);
    await Policies.findOne({session_ID: req.body.sessionID}, (err, result) => {
        if (result != null) {
            res.send(result.ulrs);
        }
    });
}

module.exports = {
    create_policy,
    get_policy_user,
    get_policy_session_id

};


