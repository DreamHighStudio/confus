import Users from '../models/users-models'

const jwt = require('jsonwebtoken');
const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const SECRET = 'RnwetTiV5bars+A33aIjzR1F4ZZGcVA%xsjd%G7U%qnnnz#GsR3N+WE7kK_ambdj';
const passportOpts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: SECRET
};

passport.use(new JwtStrategy(passportOpts, function (jwtPayload, done) {
    const expirationDate = new Date(jwtPayload.exp * 1000);
    if (expirationDate < new Date()) {
        return done(null, false);
    }
    done(null, jwtPayload);
}));

passport.serializeUser(function (user, done) {
    done(null, user.email);
});

async function login(req, res, next) {
    console.log("here");
    const {email, password} = req.body;
    const user = {
        'email': email,
        'role': 'basic'
    };
    let isValid = await authenticateUser(email, password);
    if (isValid) {
        const token = await jwt.sign(user, SECRET, {expiresIn: 36000});
        res.json({jwt: token});
    } else {
        // Error code 1002 auth failed.
        res.json(1002);
    }
}

function logout(req, res, next) {
    res.sendStatus(204);
}

async function signUp(req, res, next) {
    await Users.findOne({email: req.body.email}, async (err, result) => {
        if (result != null) {
            // Error code 1001 duplicate account found associated with this account.
            res.json(1001)
        } else {
            let hash = await Users.hashPassword(req.body.password);
            let user = new Users();
            user.name = req.body.name;
            user.email = req.body.email;
            user.password = hash;
            user.save().then(user => {
                res.json(202);
            }).catch(err => {
                res.status(400).send("Failed to create new record");
            });
        }
    });
}

function dashbored(req, res, next) {
    // console.log("dashbored");
    res.json("dashbored works");
}


async function authenticateUser(email, password) {
    let isValid = false;
    await Users.findOne({email: email}, async (err, result) => {
        if (result != null) {
            await Users.comparePasswords(password, result.password).then((res) => {
                if (res) {
                    isValid = true;
                }
            });
        }
    });
    return isValid;
}


module.exports = {
    signUp,
    login,
    logout,
    dashbored
};
