import mongoose from "mongoose";

const Schema = mongoose.Schema;

let PolicyModels = new Schema({
    policy_name: {
        type: String,
        required: true
    },
    ulrs: {
        type: [],
        required: true,
    },
    date: {
        type: String,
        required: true
    },
    session_ID:{
        type: String,
        required: true
    }
});

const Policies = mongoose.model('Policies', PolicyModels);
module.exports = Policies;
