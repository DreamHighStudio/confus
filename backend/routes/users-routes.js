// user API's
import express from 'express';

const router = express.Router();
import passport from 'passport'

// Controllers
import users_controller from '../controllers/users-controller';


router.post('/signup', users_controller.signUp);
router.post('/login', users_controller.login);
router.post('/logout', users_controller.logout);
router.get('/dashbored', passport.authenticate('jwt'), users_controller.dashbored);

module.exports = router;
