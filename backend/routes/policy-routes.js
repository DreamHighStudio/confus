// user API's
import express from 'express';

const router = express.Router();
import passport from 'passport'

// Controllers
import policy_controller from '../controllers/policy-controller';


router.post('/create_policy', passport.authenticate('jwt'), policy_controller.create_policy);
router.post('/get_policy_user', passport.authenticate('jwt'), policy_controller.get_policy_user);
router.post('/get_policy_session_id', policy_controller.get_policy_session_id);


module.exports = router;
