import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {of, Observable} from "rxjs";
import {catchError, mapTo, tap} from "rxjs/operators";
import {config} from './../../config';
import {Tokens} from '../models/tokens'
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly JWT_TOKEN = 'JWT_TOKEN';
  private loggedUser: string;

  constructor(private http: HttpClient, private router: Router) {
  }

  login(email, password) {
    const user = {
      email: email,
      password: password
    };
    return this.http.post<any>(`${config.api_uri}/users/login`, user)
      .pipe(
        tap(tokens => this.doLoginUser(user.email, tokens)),
        catchError(error => {
          alert(error.error);
          return of(false);
        }));
  }

  logout() {
    this.doLogoutUser();
  }

  isLoggedIn() {
    return !!this.getJwtToken();
  }

  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  private doLoginUser(email: string, tokens: Tokens) {
    this.loggedUser = email;
    this.storeUser();
    this.storeTokens(tokens);
  }

  private doLogoutUser() {
    this.loggedUser = null;
    this.removeTokens();
    this.router.navigate(['/login']);
  }

  private storeTokens(tokens: Tokens) {
    localStorage.setItem(this.JWT_TOKEN, tokens.jwt);
  }

  private storeUser() {
    localStorage.setItem("user",this.loggedUser);
  }

  private removeTokens() {
    localStorage.removeItem(this.JWT_TOKEN);
  }

  registerUser(name, email, passwordConfirmation) {
    const user = {
      name: name,
      email: email,
      password: passwordConfirmation,
    };
    return this.http.post(`${config.api_uri}/users/signup`, user);
  }

  createPolicy(policyName, urls, date) {
    let user = localStorage.getItem("user");
    let policy = {
      email: user,
      policyName: policyName,
      urls: urls,
      date: date,
    };
    return this.http.post(`${config.api_uri}/policy/create_policy`, policy);
  }

  getPolicy(){
    let userID = localStorage.getItem("user");
    let user = {
      email: userID,
    };
    return this.http.post(`${config.api_uri}/policy/get_policy_user`, user);
  }
}
