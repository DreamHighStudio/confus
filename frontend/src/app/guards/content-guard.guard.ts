import {Injectable} from '@angular/core';
import {CanActivate, CanLoad, Router} from '@angular/router';
import {UserService} from "../services/user.service";


@Injectable({
  providedIn: 'root'
})
export class ContentGuardGuard implements CanActivate, CanLoad {
  constructor(private userService: UserService, private router: Router) {
  }

  canActivate() {
    return this.canLoad();
  }

  canLoad() {
    console.log("can_load")
    if (!this.userService.isLoggedIn()) {
      this.router.navigate(['/login']);
    }
    return this.userService.isLoggedIn();
  }

}
