import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service'
import {FormControl, FormGroup, Validators, AbstractControl, ValidatorFn} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  fullname: string;
  email_address: string;
  password_one: string;
  confirmation_password_one: string;

  signup_form: any;

  hide_dup_acc_err: boolean = true;

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
    this.signup_form = new FormGroup({
      name: new FormControl(this.fullname, [
        Validators.required,
        Validators.pattern("^[a-zA-Z ]*$"),
      ]),
      email: new FormControl(this.email_address, [
        Validators.required,
        Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$"),
      ]),
      password: new FormControl(this.password_one, [
        Validators.required,
        Validators.minLength(8),
        // Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#'\'\$%\^&\*])(?=.{8,})"),
      ]),
      password_confirmation: new FormControl(this.confirmation_password_one, [
        Validators.required,
        Validators.minLength(8),
        this.passwordMatch(),
        // Validators.pattern("^?=.*[!@#$&*]$"),
      ])
    });
  }

  async signup() {
    if (this.signup_form.status == "VALID") {
      await this.userService.registerUser(this.fullname, this.email_address, this.confirmation_password_one).subscribe((res) => {
        // 202 no errors
        if (res == 202) {
          this.userService.login(this.email_address, this.password_one).subscribe((res) => {
            if (res != '1002') {
              this.router.navigate(['/dashbored']);
            }
          });
          this.hide_dup_acc_err = true;
        } else {
          if (res == 1001) {
            this.hide_dup_acc_err = false;
          }
        }
      });
    }
  }

  passwordMatch(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (this.password_one != control.value) {
        return {'passwordMatch': true};
      }
      return null;
    };
  }
}
