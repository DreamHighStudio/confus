import {Component, OnInit} from '@angular/core';
import {config} from "../../../config";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-dashbored',
  templateUrl: './dashbored.component.html',
  styleUrls: ['./dashbored.component.scss']
})
export class DashboredComponent implements OnInit {

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    let temp = this.http.get<any>((`${config.api_uri}/users/dashbored`)).subscribe((res) => {
      // console.log(res);
    });
  }

}
