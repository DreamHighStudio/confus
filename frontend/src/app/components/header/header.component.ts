import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isUserLoggedIn = false;
  constructor(private userServive: UserService) {
  }

  ngOnInit() {
    this.isUserLoggedIn = this.userServive.isLoggedIn();
    console.log(this.isUserLoggedIn);
  }

}
