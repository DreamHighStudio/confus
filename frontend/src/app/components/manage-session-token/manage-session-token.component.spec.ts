import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageSessionTokenComponent } from './manage-session-token.component';

describe('ManageSessionTokenComponent', () => {
  let component: ManageSessionTokenComponent;
  let fixture: ComponentFixture<ManageSessionTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageSessionTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageSessionTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
