import {Component, OnInit} from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-policies',
  templateUrl: './policies.component.html',
  styleUrls: ['./policies.component.scss'],
})
export class PoliciesComponent implements OnInit {
  closeResult: string;
  policyName: string;
  urlInput: string;
  urls = [];

  allPolicies: any;

  constructor(private modalService: NgbModal, private userService: UserService, private router: Router) {
  }

  ngOnInit() {
    this.userService.getPolicy().subscribe((res) => {
      this.allPolicies = res;
      console.log(res)
    })
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  addUrls(url) {
    this.urls.push(url);
    this.urlInput = "";
  }

  deleteUrl(url) {
    let index = this.urls.indexOf(url);
    this.urls.splice(index, 1);
  }

  createPolicy() {
    this.modalService.dismissAll("success");
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();

    let date = mm + '/' + dd + '/' + yyyy;
    console.log(date);
    this.userService.createPolicy(this.policyName, this.urls, date).subscribe((res) => {
      this.router.navigate(['/policies']);
      location.reload();
    })
  }
}
