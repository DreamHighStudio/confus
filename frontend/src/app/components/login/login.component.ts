import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;

  login_form: any;

  hide_login_invalid: boolean = true;

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
    this.login_form = new FormGroup({
      email: new FormControl(this.email, [
        Validators.required,
        Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$"),
      ]),
      password: new FormControl(this.password, [
        Validators.required,
      ]),
    })
  }

  login() {
    if (this.login_form.status == "VALID") {
      this.userService.login(this.email, this.password).subscribe((res) => {
        if(res == '1002'){
          this.hide_login_invalid = false;
        }else{
          this.router.navigate(['/dashbored']);
          this.hide_login_invalid = true;
        }
      });
    }
  }

}
