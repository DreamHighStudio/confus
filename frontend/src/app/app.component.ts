import {Component} from '@angular/core';
import {Router, NavigationEnd} from "@angular/router";
import construct = Reflect.construct;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'Confus';
  showHeaderFooter: boolean;
  constructor(private router: Router) {
  }

  ngOnInit() {
    // this.router.events.subscribe((event) => {
    //   if(event instanceof NavigationEnd){
    //     this.showHeaderFooter = (event.url !== '/page-not-found');
    //   }
    // });
  }

}
