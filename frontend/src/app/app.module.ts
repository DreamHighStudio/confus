import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SignupComponent} from './components/signup/signup.component';
import {LoginComponent} from './components/login/login.component';
import {DashboredComponent} from './components/dashbored/dashbored.component';
import {ManagePoliciesComponent} from './components/manage-policies/manage-policies.component';
import {ManageSessionTokenComponent} from './components/manage-session-token/manage-session-token.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";

import {HomePageComponent} from './components/home-page/home-page.component';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {DownloadComponent} from './components/download/download.component';
import {HelpComponent} from './components/help/help.component';
import {PricesComponent} from './components/prices/prices.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';

import {UserService} from './services/user.service'
import {ValidateService} from "./services/validate.service";
import {TokenInterceptor} from "./providers/token-interceptor";
import {ContentGuardGuard} from "./guards/content-guard.guard";
import {AuthGuard} from "./guards/auth.guard";
import { PoliciesComponent } from './components/policies/policies.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    DashboredComponent,
    ManagePoliciesComponent,
    ManageSessionTokenComponent,
    HomePageComponent,
    HeaderComponent,
    FooterComponent,
    DownloadComponent,
    HelpComponent,
    PricesComponent,
    PageNotFoundComponent,
    PoliciesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  providers: [
    UserService,
    ValidateService,
    ContentGuardGuard,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
