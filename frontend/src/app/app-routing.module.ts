import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomePageComponent} from "./components/home-page/home-page.component";
import {LoginComponent} from "./components/login/login.component";
import {SignupComponent} from "./components/signup/signup.component";
import {DownloadComponent} from "./components/download/download.component";
import {PricesComponent} from "./components/prices/prices.component";
import {HelpComponent} from "./components/help/help.component";
import {PageNotFoundComponent} from "./components/page-not-found/page-not-found.component";
import {DashboredComponent} from "./components/dashbored/dashbored.component";
import {AuthGuard} from "./guards/auth.guard";
import {ContentGuardGuard} from "./guards/content-guard.guard";
import {PoliciesComponent} from "./components/policies/policies.component";


const routes: Routes = [
  {
    path: 'home',
    component: HomePageComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'signup',
    component: SignupComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'download',
    component: DownloadComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'price',
    component: PricesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'help',
    component: HelpComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'dashbored',
    component: DashboredComponent,
    canActivate:[ContentGuardGuard],
    canLoad: [ContentGuardGuard],
  },{
    path: 'policies',
    component: PoliciesComponent,
    canActivate:[ContentGuardGuard],
    canLoad: [ContentGuardGuard],
  },
  // {path:'page-not-found', component:PageNotFoundComponent},
  // {path: '**', redirectTo: 'page-not-found'},
  {path: '', redirectTo: 'home', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
