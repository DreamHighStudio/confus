chrome.runtime.onInstalled.addListener(function () {
    chrome.storage.sync.set({color: '#3aa757'}, function () {
        console.log("The color is green.");
    });
    // var tempid = "8tn6gjjik3787c2ooi2oyhshkr6bu9rx";
    // const req = new XMLHttpRequest();
    // const baseUrl = "http://localhost:4000/policy/get_policy_session_id";
    // const urlParams = `sessionID=${tempid}`;
    //
    // req.open("POST", baseUrl, true);
    // req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // req.send(urlParams);
    //
    // req.onreadystatechange = function () { // Call a function when the state changes.
    //     if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
    //         // console.log("Got response 200!");
    //         //console.log(req.response);
    //         var bannedURLs = JSON.parse(req.response);
    //         console.log(bannedURLs);
    //         chrome.storage.sync.set({bannedUrl: bannedURLs}, function () {
    //             // console.log("Get Outa here");
    //         });
    //     }
    // };
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [new chrome.declarativeContent.PageStateMatcher({
                pageUrl: {hostEquals: 'developer.chrome.com'},
            })
            ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });
});

chrome.storage.sync.get('bannedUrl',  (data) => {
    console.log(data.bannedUrl);
    chrome.webRequest.onBeforeRequest.addListener(
        function(details) {
            for (let i of data.bannedUrl) {
                if (details.url.indexOf(i) != -1) {
                    return {cancel: true};
                }
            }
        },
        {urls: ["<all_urls>"]},
        ["blocking"]);
});
